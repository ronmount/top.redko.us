-- +goose Up
-- +goose StatementBegin
CREATE TABLE IF NOT EXISTS top_updates(
    last_update TIMESTAMP WITH TIME ZONE NOT NULL
);
-- +goose StatementEnd

-- +goose Down
-- +goose StatementBegin
DROP TABLE IF EXISTS top_updates;
-- +goose StatementEnd
