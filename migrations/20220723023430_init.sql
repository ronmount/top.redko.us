-- +goose Up
-- +goose StatementBegin
CREATE TABLE IF NOT EXISTS users (
    uid INTEGER PRIMARY KEY,
    login TEXT,
    level REAL
);
-- +goose StatementEnd

-- +goose Down
-- +goose StatementBegin
DROP TABLE IF EXISTS users;
-- +goose StatementEnd
