package service

import (
	"context"
	"fmt"
	"github.com/jedib0t/go-pretty/v6/table"
	"gitlab.com/ronmount/top.redko.us/internal/model"
	"gitlab.com/ronmount/top.redko.us/internal/store"
	"strings"
)

type TopService struct {
	Store *store.Store
}

func NewTopService(store *store.Store) *TopService {
	return &TopService{
		Store: store,
	}
}

func (i *TopService) GetTop(ctx context.Context) (*string, error) {
	users, err := i.Store.GetUsers(ctx)
	if err != nil {
		return nil, err
	}

	lastUpdate, err := i.Store.GetLastUpdateTime(ctx)
	if err != nil {
		return nil, err
	}

	s := strings.Builder{}
	s.WriteString(fmt.Sprintf("Last update: %s\n", lastUpdate.Time.Format("2006-01-02 15:04:05")))
	s.WriteString(fmt.Sprint("Repo: gitlab.com/ronmount/top.redko.us\n\n"))
	s.WriteString(i.getTopFormatted(users))
	result := s.String()

	return &result, nil
}

func (i *TopService) getTopFormatted(users []model.User) string {
	t := table.NewWriter()
	t.AppendHeader(table.Row{"№", "login", "level"})
	for j, user := range users {
		t.AppendRow([]interface{}{j + 1, user.Login, fmt.Sprintf("%.2f", user.Level)})
	}
	result := t.Render()
	return result
}
