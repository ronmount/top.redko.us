package service

import "gitlab.com/ronmount/top.redko.us/internal/store"

type AppService struct {
	*TopService
}

func NewAppService(store *store.Store) *AppService {
	return &AppService{
		NewTopService(store),
	}
}
