package store

import (
	"context"
	"database/sql"
	"gitlab.com/ronmount/top.redko.us/internal/model"
)

func (s *Store) GetUsers(ctx context.Context) ([]model.User, error) {
	var users []model.User
	err := s.db.SelectContext(ctx, &users, "SELECT * FROM users order by level desc, login asc")
	if err != nil {
		return nil, err
	}

	return users, nil
}

func (s *Store) GetLastUpdateTime(ctx context.Context) (*sql.NullTime, error) {
	var lastUpdateTime sql.NullTime
	err := s.db.GetContext(ctx, &lastUpdateTime, "SELECT last_update FROM top_updates limit 1")
	if err != nil {
		return nil, err
	}

	return &lastUpdateTime, nil
}
