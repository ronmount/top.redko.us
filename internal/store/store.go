package store

import (
	"github.com/jmoiron/sqlx"
	_ "github.com/lib/pq"
	"gitlab.com/ronmount/top.redko.us/internal/config"
	"log"
)

type Store struct {
	db *sqlx.DB
}

func NewStore() *Store {
	dataSourceName := config.GetPostgresDataSource()
	db, err := sqlx.Connect("postgres", dataSourceName)
	if err != nil {
		log.Fatalf("failed to connect to postgres: %v", err)
	}
	if db.Ping() != nil {
		log.Fatal("Could not connect to database")
	}

	return &Store{db: db}
}

func (s *Store) Close() error {
	return s.db.Close()
}
