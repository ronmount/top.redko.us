package config

import (
	"fmt"
	"log"
	"os"
)

func GetPostgresDataSource() string {
	user := os.Getenv("POSTGRES_USER")
	password := os.Getenv("POSTGRES_PASSWORD")
	host := os.Getenv("POSTGRES_HOST")
	port := os.Getenv("POSTGRES_PORT")
	dbname := os.Getenv("POSTGRES_DB")

	if user == "" {
		log.Fatal("POSTGRES_USER is not set")
	}
	if password == "" {
		log.Fatal("POSTGRES_PASSWORD is not set")
	}
	if host == "" {
		log.Fatal("POSTGRES_HOST is not set")
	}
	if port == "" {
		log.Fatal("POSTGRES_PORT is not set")
	}
	if dbname == "" {
		log.Fatal("POSTGRES_DB is not set")
	}

	return fmt.Sprintf("postgres://%s:%s@%s:%s/%s", user, password, host, port, dbname)
}
