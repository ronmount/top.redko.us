package model

type User struct {
	ID    int     `db:"uid"`
	Login string  `db:"login"`
	Level float64 `db:"level"`
}
