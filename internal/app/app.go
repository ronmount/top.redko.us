package app

import (
	"context"
	"github.com/gin-gonic/gin"
	"gitlab.com/ronmount/top.redko.us/internal/service"
	"gitlab.com/ronmount/top.redko.us/internal/store"
	"log"
	"net/http"
	"os"
	"os/signal"
	"syscall"
	"time"
)

type App struct {
	Store   *store.Store
	Router  *gin.Engine
	Service *service.AppService
}

func NewApp(store *store.Store) *App {
	return &App{
		Store:   store,
		Router:  gin.Default(),
		Service: service.NewAppService(store),
	}
}

func (i *App) Run() {
	ctx := context.Background()
	i.registerRoutes(ctx)

	srv := &http.Server{
		Addr:    ":8080",
		Handler: i.Router,
	}
	go func() {
		if err := srv.ListenAndServe(); err != nil && err != http.ErrServerClosed {
			log.Fatalf("listen: %s\n", err)
		}
	}()

	quit := make(chan os.Signal)
	signal.Notify(quit, syscall.SIGINT, syscall.SIGTERM)
	<-quit
	log.Println("Shutdown Server ...")

	ctx, cancel := context.WithTimeout(context.Background(), 5*time.Second)
	defer cancel()
	if err := srv.Shutdown(ctx); err != nil {
		log.Fatalf("Server Shutdown error: %#v", err)
	}
	if err := i.Store.Close(); err != nil {
		log.Fatalf("Store Close error: %#v", err)
	}

	select {
	case <-ctx.Done():
		log.Println("timeout of 5 seconds.")
	}
	log.Println("Server exiting")
}

func (i *App) registerRoutes(ctx context.Context) {
	i.Router.GET("/", func(c *gin.Context) {
		top, err := i.Service.GetTop(ctx)
		if err != nil {
			c.JSON(http.StatusInternalServerError, gin.H{"error": err.Error()})
			return
		}

		c.Writer.WriteHeader(http.StatusOK)
		c.Writer.Write([]byte(*top))
	})
}
