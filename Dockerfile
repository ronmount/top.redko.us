FROM golang:1.18.4-alpine3.15 as builder
COPY go.mod go.sum /go/src/gitlab.com/ronmount/top.redko.us/
WORKDIR /go/src/gitlab.com/ronmount/top.redko.us
RUN go mod download
COPY . /go/src/gitlab.com/ronmount/top.redko.us
RUN CGO_ENABLED=0 go build -a -installsuffix cgo -o build/intra ./cmd/main.go

FROM alpine
RUN apk add --no-cache ca-certificates && update-ca-certificates
COPY --from=builder /go/src/gitlab.com/ronmount/top.redko.us/build/intra /usr/bin/intra
COPY --from=builder /go/src/gitlab.com/ronmount/top.redko.us/.env .
EXPOSE 8080 8080
ENTRYPOINT ["/usr/bin/intra"]