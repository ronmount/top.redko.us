package main

import (
	"gitlab.com/ronmount/top.redko.us/internal/app"
	"gitlab.com/ronmount/top.redko.us/internal/store"
)

func main() {
	a := app.NewApp(store.NewStore())
	a.Run()
}
